(ns guilloche.core
  (:require [complex.core :as c])
  (:require [gnuplot.core :as g])
  (:gen-class))

(defn timeseq [timestep] (map #(* timestep %) (range)))

;;; thin wrapper around complex numbers
(defn make-point [x y] (c/complex x y))
(defn x [p] (c/real-part p))
(defn y [p] (c/imaginary-part p))

;;; make printing complex numbers less eye-gouging
(defmethod print-method org.apache.commons.math3.complex.Complex
  [c ^java.io.Writer w]
  (let [real (x c) imag (y c) fmt "%.3f"]
    (.write w (apply str ["(" (format fmt real) ", " (format fmt imag) "i)"]))))

;;; geometric primitives and transformations
(defn circle-intersection
  "Compute intersection points between 2 circles with center points and radii P1,
  R1 and P2, R2, respectively."
  [p1 r1 p2 r2]
  (let [[x1     y1     x2     y2     d]
        [(x p1) (y p1) (x p2) (y p2) (c/abs (c/- p2 p1))]]
    (cond
      ;; circles too far apart, no solutions
      (> d (+ r1 r2))
      (throw (Exception. "Circles too far apart, no solutions."))
      ;; one circle contained within the other, no solutions
      (< d (Math/abs (- r2 r1)))
      (throw (Exception. "One circle fully contained, no solutions."))
      ;; coincident circles, infinite solutions
      (and (= d 0) (= r1 r2))
      (throw (Exception. "Circles coincide, infinite solutions."))
      ;; common case
      :else
      (let [a (/ (+ (Math/pow r1 2)
        	    (- (Math/pow r2 2))
        	    (Math/pow d 2))
        	 (* 2 d))
            h (Math/sqrt (- (Math/pow r1 2)
        	            (Math/pow a 2)))
            p3 (c/+ p1
        	    (c/* (/ a d)
        	         (c/- p2 p1)))
            [x3 y3] [(x p3) (y p3)]]
        [(make-point (+ x3 (* (/ h d) (- y2 y1)))
        	     (- y3 (* (/ h d) (- x2 x1))))
         (make-point (- x3 (* (/ h d) (- y2 y1)))
        	     (+ y3 (* (/ h d) (- x2 x1))))]))))

(defn rescale-vector
  "Compute the new endpoint of the vector between INIT and TERM points such that
  it has the specified total LENGTH between old INIT and the computed endpoint."
  ([init term new-length]
   (rescale-vector init (c/abs (c/- term init)) term new-length))
  ([init old-length term new-length]
   (c/+ init
        (c/* (c/- term init)
             (/ new-length old-length)))))

(defn rotate-around-origin [point angle]
  (c/* point (c/exp (c/complex 0 angle))))

(defn closest-to-previous [dir]
  (letfn [(point->vector [p] [(x p) (y p)])
          (vector->point [v] (apply make-point v))]
    (let [angles {:n 1/2 :e 0 :s -1/2 :w 1 :ne 1/4 :se -1/4 :sw -3/4 :nw 3/4}
          previous (atom (rotate-around-origin (make-point (Math/pow 10 4) 0)
                                               (dir angles)))]
      (fn [points]
        (swap! previous
               (fn [previous]
                 (reduce #(if (< (c/abs (c/- %1 previous))
                                 (c/abs (c/- %2 previous)))
                            %1
                            %2)
                         points)))))))

;;; Component definitions
(defn make-driver [center radius omega phase] 
  (fn [time]
    (let [total-phase (+ (* omega time) phase)]
      [(c/+ (rotate-around-origin (make-point radius 0) total-phase)
            (if (fn? center) (first (center time)) center))])))

(defn pair-drivers [d1 d2]
  (fn [time] (apply conj (d1 time) (d2 time))))

(defn make-x-rods
  ([r1-pre r2-pre r1-post r2-post]
   (make-x-rods :n r1-pre r2-pre r1-post r2-post))
  ([dir r1-pre r2-pre r1-post r2-post]
   (let [selector (closest-to-previous dir)]
     (fn [[p1 p2]]
       (let [joint (selector (circle-intersection p1 r1-pre p2 r2-pre))
             rod-1-end (rescale-vector p1 joint (+ r1-pre r1-post))
             rod-2-end (rescale-vector p2 joint (+ r2-pre r2-post))]
         [rod-2-end rod-1-end])))))

(defn make-angle-rods
  ([r1 r2]
   (make-angle-rods :n r1 r2))
  ([dir r1 r2]
   (let [selector (closest-to-previous dir)]
     (fn [[p1 p2]]
       [(selector (circle-intersection p1 r1 p2 r2))]))))

(defn plot-point-sequence [n pseq]
  (let [points (map (comp #(vector (x %) (y %)) first)
                    (take n pseq))]
    (g/raw-plot! [[:reset]
                  [:unset :border]
                  [:unset :xtics]
                  [:unset :ytics]
                  [:set :key :off]
                  [:set :size :ratio 1]
                  [:set :terminal :pdfcairo :background :rgb "black"]
                  [:set :output "output/demo.pdf"]
                  [:plot "-" :with :lines :lc "gray90"]]
                 [points])))

(defn demo []
  (let [t (timeseq 0.05)
      dl0 (make-driver (make-point 0 0) 1 0.5 (* Math/PI 1/2))
      dr0 (make-driver (make-point 5 0) 1 -0.49 0)
      dl1 (make-driver dl0 0.7 0.002 1.5)
      dr1 (make-driver dr0 0.61 -0.001 1.4)
      driver-pair (pair-drivers dl1 dr1)
      x-rods (make-x-rods :nw 4 4 4 4)
      angle-rods (make-angle-rods :nw 4 4)]
  (->> t
       (map driver-pair)
       (map x-rods)
       (map angle-rods)
       (plot-point-sequence 20000))))


