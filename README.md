

# Description

Guilloche is a Clojure library providing the means to create guilloche-like
drawings. Original inspiration came from a [youtube video](https://www.youtube.com/watch?v=2DjvtjgRdGA), showcasing the
"ptolemaios" drawing machine. Although the name guilloche is probably not the
most suitable (compared to e.g. spirograph or harmonograph), it was chosen in
order to convey the situation where a "very precise, intricate and repetitive
pattern is mechanically engraved into an underlying material via engine
turning" (for example, [this image](https://gitlab.com/agspathis/guilloche/raw/master/output/demo.pdf) is produced by the `demo` function).

The machine topology is described component-wise. Each component is either a
driver (mapping points from time to 2D-space) or a transformer (mapping points
from 2D-space to 2D-space). The time sequence is then threaded through the
drivers and transformers to yield the resulting point sequence, which is
subsequently plotted via [Gnuplot](http://www.gnuplot.info/).


# License

Copyright © 2018 Aris Spathis

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.

